%module timeClock

%{
#define SWIG_FILE_WITH_INIT
#include "timeClock.h"
#include <stdio.h>
#include <time.h>
%}

extern double monotonic();
extern void nanoSleep( long tv_sec,long tv_nsec );

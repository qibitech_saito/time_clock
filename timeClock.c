#include <stdio.h>
#include <time.h>
#include "timeClock.h"
double monotonic()
{
  struct timespec ts;
  double sec_nsec;
  //
  clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
  sec_nsec = ts.tv_sec + ts.tv_nsec / 1000000000.0;
  //printf("time:    %10ld.%09ld  %f CLOCK_MONOTONIC_RAW\n", ts.tv_sec, ts.tv_nsec, sec_nsec);
  return sec_nsec;
}
void nanoSleep( long tv_sec,long tv_nsec )
{
  struct timespec ts;
  ts.tv_sec  = tv_sec;
  ts.tv_nsec = tv_nsec;
  nanosleep( &ts,NULL );
}

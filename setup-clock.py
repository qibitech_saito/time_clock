from distutils.core import setup, Extension

module1 = Extension('_timeClock',
                    sources = ['timeClock_wrap.c', 'timeClock.c'])

setup (name = 'timeClock',
       version = '1.0',
       description = 'This is a timeClock package',
       ext_modules = [module1],
       py_modules = ["timeClock"],)

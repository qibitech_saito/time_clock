Python2系では、time.monotoic()が無いため、cラッパを作成する 2020/03/30
----------------------------------------------------------------------

   timeClock.monotonic()   # モノトニッククロックの値をnsecで取得する

   timeClock.nanoSleep( 0,10000 )  # nsec遅延
                        | |
                        | +-- nsec
                        +-- sec
  注意）
    プラットホームによって_timeClock.soが異なります。(2)のpython-c
    ラッパー作成をして下さい。

(1)cで関数を作成する

   timeClock.c    -- 関数
   timeClock.h    -- ヘッダ
   timeClock.i    -- Pythonラッパ用のextern定義

(2)python-cラッパー作成
   swig でPythonラッパを作成する。
   swigは、 sudo apt install swig  でインストールできます。

$ swig -python timeClock.i

  timeClock_wrap.c  が作成される

$ python setup-clock.py build_ext --inplace

  timeClock.py,_timeClock.so が作成される

(3)使い方
  timeClock.py,_timeClock.soをカレントに置く

>>> import timeClock
>>> print timeClock.monotonic()
6357.67538105

>>> timeClock.nanoSleep( 0,10000 )

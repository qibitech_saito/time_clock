#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import time
import timeClock
import rospy
#
def main():
    rospy.init_node('time-talker', anonymous=True)
    r = rospy.Rate(10000) # 1000hz-1msce, 10000hz-100μsec
    print 'timeClock.monotonic',',','rospy.get_time'
    while not rospy.is_shutdown():
        # rospy.get_time() -- 1msecまで
        # clockMonotonic.monotonic() -- 0.1msecまで 100%精度
        #  - 0.01msec -- 30%精度 <-- r.sleep()を入れなければ、10μsecもok
        #    ただし、マシンスペックに依存します。
        #      rasPI-4 -- 100μsec まで
        #      i7-4790 CPU @ 3.60GHz -- 10μsec まで
        #
        print timeClock.monotonic(),',',rospy.get_time()
        #
        #time.sleep(0.0001)  # この遅延は、 1msecまで
        #                    +-- sec
        #                    | +-- nsec (10000nsec=10μsec)
        #timeClock.nanoSleep( 0,10000 )
        #timeClock.nanoSleep( 0,40000 )    # 40μsec で 100μsecのtimeClock.monotonic()精度
        # timeClock.nanoSleep は、r.sleep と略同じ精度
        #r.sleep()
#
if __name__ == '__main__':
    #
    try:
      main()
    except rospy.ROSInterruptException: pass

